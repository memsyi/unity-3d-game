using UnityEngine;
using System.Collections;

public class BasicBehaviour : MonoBehaviour {
	private Transform _myTransform;
	
	public void Awake() {
		_myTransform = transform;
	}

	public Transform transform {
		get {
			return _myTransform;
		}
	}
}