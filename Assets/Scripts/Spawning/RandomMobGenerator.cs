using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomMobGenerator : MonoBehaviour {
	public  enum State {
		Idle,
		Initialize,
		Setup,
		Spawn
	}
	
	public GameObject[] mobPrefabs;
	public GameObject[] spawnPoints;
	
	public State state;
	
	void Awake() {
		state = State.Initialize;	
	}

	// Use this for initialization
	IEnumerator Start () {
		while(true) {
			switch(state) {
			case State.Initialize:
				Initialize();
				break;
			case State.Setup:
				Setup();
				break;
			case State.Spawn:
				Spawn();
				break;
			}
			
			yield return 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private void Initialize() {
		if (!CheckForMobPrefabs()) {
			Debug.LogError("There are no mob prefabs attached to spawn script! ");
			return;	
		}
		
		if (!CheckForSpawnPoints()) {
			Debug.LogError("There are no spawn points attached to spawn script!");
			return;
		}
		
		state = State.Setup;
	}
	
	private void Setup() {
		state = State.Spawn;	
	}
	
	private void Spawn() {
		List<GameObject> gos = AvailableSpawnPoints();
		
		foreach(GameObject spawnPoint in gos) {
			GameObject go = Instantiate(mobPrefabs[Random.Range(0, mobPrefabs.Length)],
										spawnPoint.transform.position,
										Quaternion.identity
										) as GameObject;
			go.transform.parent = spawnPoint.transform;
		}
		
		state = State.Idle;	
	}
	
	private bool CheckForMobPrefabs() {
		return mobPrefabs.Length > 0;
	}
	
	private bool CheckForSpawnPoints() {
		return spawnPoints.Length > 0;	
	}
	
	private List<GameObject> AvailableSpawnPoints() {
		List<GameObject> gos = new List<GameObject>();
		
		foreach (GameObject spawnPoint in spawnPoints) {
			if (spawnPoint.transform.childCount == 0) {
				Debug.Log("Spawn Point Available at " + spawnPoint.transform.position);
				gos.Add(spawnPoint);
			}
		}
		
		return gos;
	}
}
