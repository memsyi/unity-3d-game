using UnityEngine;
using System.Collections;

public class RedGizmo : MonoBehaviour {

	public void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawCube(transform.position, new Vector3(3,3,3));
	}
}
