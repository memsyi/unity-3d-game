/// <summary>
/// This script simply destroys gameobject on game start. Might be handy for creation of new stuff
/// or debugging, when u want to add for example a camera, that will be no longer needed once u start
/// the game.
/// </summary>

using UnityEngine;
using System.Collections;

public class Destroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
