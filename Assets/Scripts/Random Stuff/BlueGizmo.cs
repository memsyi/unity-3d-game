using UnityEngine;
using System.Collections;

public class BlueGizmo : MonoBehaviour {
	public float size = 3;

	public void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		Gizmos.DrawCube(transform.position, new Vector3(size,size,size));
	}
}
