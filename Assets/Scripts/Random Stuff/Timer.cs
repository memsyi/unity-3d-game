using UnityEngine;

public class Timer {
	private float _startTime = 0;
	private float _length;
	
	public Timer(float length=0) {
		_length = length;	
	}
	
	public void Start() {
		_startTime = Time.time;	
	}
	
	public void Start(float t) {
		_length = t;
		_startTime = Time.time;
	}
	
	public float Current() {
		return Mathf.Max(_length - (Time.time - _startTime), 0);
	}
}

