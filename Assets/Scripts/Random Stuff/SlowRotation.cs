using UnityEngine;
using System.Collections;

public class SlowRotation : MonoBehaviour {
	private Transform myTransform;
	
	void Awake() {
		myTransform = transform;	
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		myTransform.RotateAround(Vector3.up, Time.deltaTime / 5);
	}
}
