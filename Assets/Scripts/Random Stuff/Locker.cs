using System;
using UnityEngine;

public static class Locker {
	public static bool Check(string name) {
		return GameObject.Find (name) != null;
	}
	
	public static void Lock(string name, float t=0) {
		GameObject tmp = new GameObject(name);
		if (t > 0) GameObject.Destroy(tmp, t);
	}
	
	public static void Unlock(string name, float t=0) {
		GameObject.Destroy(GameObject.Find (name), t);
	}
	
	public static bool CheckAndLock(string name, float t) {
		if (Check(name)) return true;
		Lock (name, t);
		return false;
	}
}

