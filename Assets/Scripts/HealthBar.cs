using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {
	public int maxHealth = 100;
	public int curHealth = 100;
	
	public float healthBarLength;
	
	private Texture2D t;
	private GUIStyle gs;
	
	private bool visible;
	
	// Use this for initialization
	void Start () {
		healthBarLength = Screen.width / 2;
		t = new Texture2D(1, 1);
		t.SetPixel(1, 1, Color.red);
		t.wrapMode = TextureWrapMode.Repeat;
		t.Apply();
		gs = new GUIStyle();
		gs.normal.background = t;
	}
	
	// Update is called once per frame
	void Update () {
		AdjustCurrentHealth(0);
	}
	
	void OnGUI() {
		if (!this.visible) return;
		GUI.Label(new Rect(10, 10, Screen.width / 2, 20), t, gs);
		GUI.Box(new Rect(10, 10, healthBarLength, 20), curHealth + "/" + maxHealth);	
	}
	
	public void setVisible(bool visible) {
		this.visible = visible;	
	}
	
	public void AdjustCurrentHealth(int adj) {
		curHealth += adj;
		
		if (curHealth < 0) 
			curHealth = 0;
		if (curHealth > maxHealth)
			curHealth = maxHealth;
		
		healthBarLength = (Screen.width / 2) * (curHealth / (float)maxHealth);
	}
}
