using UnityEngine;
using System.Collections;

public class Mob : SparkUser {
	
	new void Awake() {
		base.Awake ();
		//Name = "Evil Mob";
	}
	
	// Use this for initialization
	void Start () {
		//transform.FindChild("Name").GetComponent<TextMesh>().text = Name;
		GetPrimaryAttribute((int)AttributeName.Constitution).BaseValue = 20;
		StatUpdate();
		GetVital((int)VitalName.Health).CurValue = /*(int)(Time.realtimeSinceStartup * 99999) %*/ GetVital((int)VitalName.Health).AdjustedBaseValue;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	//public override void DisplayHealth() {
	//	Messenger<int, int>.Broadcast(MobHealthBar.MESSAGE, GetVital((int)VitalName.Health).CurValue, GetVital((int)VitalName.Health).AdjustedBaseValue);
	//}
}
