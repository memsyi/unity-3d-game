using UnityEngine;
using System.Collections;

public class CharacterAnimator : MonoBehaviour {
	
	private Animation _animation;
	
	// Use this for initialization
	void Start () {
		_animation = animation;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlayRun() {
		_animation.wrapMode = WrapMode.Loop;
		Play("run");
	}
	
	public void CrossFadeRun() {
		_animation.wrapMode = WrapMode.Loop;
		CrossFade("run");
	}
	
	public void PlayIdle() {
		_animation.wrapMode = WrapMode.Loop;
		Play("idle");
	}
	
	public void CrossFadeIdle() {
		_animation.wrapMode = WrapMode.Loop;
		CrossFade("idle");
	}
	
	public void QueueIdle() {
		_animation.wrapMode = WrapMode.Loop;
		Queue("idle");
	}
	
	public void PlaySteady() {
		_animation.wrapMode = WrapMode.Loop;
		Play("steady");
	}
	
	public void PlayDie() {
		_animation.wrapMode = WrapMode.Once;
		Play("die");
	}
	
	public void PlayDance() {
		_animation.wrapMode = WrapMode.Loop;
		Play("dance");
	}
	
	public void PlayMelee() {
		_animation.wrapMode = WrapMode.Once;
		Play("melee");
	}
	
	public void CrossFadeMelee() {
		_animation.wrapMode = WrapMode.Once;
		CrossFade("melee");
	}
	
	public void PlayGetHit() {
		_animation.wrapMode = WrapMode.Once;
		Play("gethit");
	}
	
	public void CrossFadeGetHit() {
		_animation.wrapMode = WrapMode.Once;
		CrossFade("gethit");
	}
	
	public void CrossFadeJump() {
		_animation.wrapMode = WrapMode.Once;
		CrossFade("jump");
	}
	
	public bool IsLooping() {
		return _animation.wrapMode == WrapMode.Loop;
	}
	
	private void Play(string animationName) {
		_animation.Play(animationName);	
	}
	
	private void CrossFade(string animationName) {
		_animation.CrossFade(animationName);	
	}
	
	private void Blend(string animationName) {
		_animation.Blend(animationName);	
	}
	
	private void Queue(string animationName) {
		_animation.PlayQueued(animationName);	
	}
}
