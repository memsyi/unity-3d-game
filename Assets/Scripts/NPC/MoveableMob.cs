using UnityEngine;
using System.Collections;

// TODO: Add gravity handling. At the moment easiest way to achieve it is to add "Character Motor"
// script and disable control of character.

[RequireComponent (typeof(CharacterController))]
public class MoveableMob : MonoBehaviour {
	
	private Transform _target;
	private Transform _myTransform;
	private float _rotationSpeed;
	private float _movementSpeed;
	private CharacterController _characterController;
	private RotationMode _rotationMode;
	private MovementMode _movementMode;
	
	public float RotationSpeed {
		get {
			return _rotationSpeed;	
		}
		set {
			_rotationSpeed = value;	
		}
	}
	
	public float MovementSpeed {
		get {
			return _movementSpeed;	
		}
		set {
			_movementSpeed = value;	
		}
	}
	
	void Awake() {
		_myTransform = transform;
		_target = _myTransform;
		_movementSpeed = 1;
		_rotationSpeed = 1;
		_characterController = GetComponent<CharacterController>();
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (_myTransform.Equals(_target) == false) {
			// Look at target
			if (_rotationMode == RotationMode.TowardsTarget) _myTransform.rotation = Quaternion.Slerp(_myTransform.rotation, Quaternion.LookRotation(_target.position - _myTransform.position), _rotationSpeed * Time.deltaTime);
			else if (_rotationMode == RotationMode.SameAsTarget) {
				_myTransform.rotation = Quaternion.Slerp(_myTransform.rotation, _target.rotation, Time.deltaTime * _rotationSpeed);	
			}
			// Move towards target
			if (_movementMode == MovementMode.Move && _target.position.Equals(_myTransform.position) == false) {		
				Vector3 direction = _target.position - _myTransform.position;
				direction = direction.normalized / 10f * _movementSpeed;
				_characterController.Move(direction);
				//_myTransform.position += _myTransform.forward * _movementSpeed * Time.deltaTime; 
			}
		}
	}
	
	public void Move(Transform target) {
		_target = target;
		_rotationMode = RotationMode.SameAsTarget;
		_movementMode = MovementMode.Move;
	}
	
	public void Stop() {
		_target = _myTransform;
		_rotationMode = RotationMode.SameAsTarget;
		_movementMode = MovementMode.Stand;
	}
	
	public void RotateTowards(Transform target) {
		_target = target;
		_rotationMode = RotationMode.TowardsTarget;
		_movementMode = MovementMode.Stand;
	}
	
	public void MoveTowards(Transform target) {
		_target = target;
		_rotationMode = RotationMode.TowardsTarget;
		_movementMode = MovementMode.Move;
	}
}

enum RotationMode {
	TowardsTarget,
	SameAsTarget
}

enum MovementMode {
	Move,
	Stand
}