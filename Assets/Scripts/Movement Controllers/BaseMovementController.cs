using UnityEngine;
using System.Collections.Generic;

[RequireComponent (typeof(CharacterController))]
public class BaseMovementController : MonoBehaviour {
	//private ControllerAction _action = null;
	private float _baseSpeed;
	private float _currentSpeed;
	private float _rotationSpeed;
	private Dictionary<float, int> _speedModifiers;
	private Transform _transform;
	private CharacterController _charController;
	private Vector3 _motion;
	
	void Awake() {
		_motion = Vector3.zero;
		_transform = transform;
		_speedModifiers = new Dictionary<float, int>();
	}
	
	public void Start() {
		_charController = GetComponent<CharacterController>();	
	}
	
	public float BaseMovementSpeed {
		get {
			return _baseSpeed;	
		}
		set {
			_currentSpeed = _baseSpeed = value;
			foreach (KeyValuePair<float, int> pair in _speedModifiers) {
				_currentSpeed *= Mathf.Pow(pair.Key, pair.Value);
			}
		}
	}
	
	public float RotationSpeed {
		get {
			return _rotationSpeed;	
		}
		set {
			_rotationSpeed = value;	
		}
	}
	
	public float CurrentMovementSpeed {
		get {
			return _currentSpeed;
		}
	}
	
	public void AddSpeedModifier(float mod) {
		if (!_speedModifiers.ContainsKey(mod)) {
			_speedModifiers[mod] = 0;
		}
		_speedModifiers[mod]++;
		_currentSpeed *= mod;
	}
	
	public void RemoveSpeedModifier(float mod) {
		_speedModifiers[mod]--;
		_currentSpeed /= mod;
	}
	
	public void MoveTowardsTarget(Transform target) {
		_motion += (target.position - _transform.position).normalized * _currentSpeed * Time.deltaTime;
		_transform.rotation = Quaternion.LookRotation(target.position - _transform.position);
	}
	
	public void MoveTowardsTarget(GameObject target) {
		MoveTowardsTarget(target.transform);
	}
	
	/* TODO: Do we really want that? that would probably involve setting _action to null in every other function...
	public void MoveAndReachTarget(Transform target) {
		_action = new MoveAction(new LinkedList<Transform>(new List<Transform>{target}));
	}
	
	public void MoveAndReachTarget(GameObject target) {
		MoveTowardsTarget(target.transform);
	}*/
	
	public void MoveTowardsTargetIgnoreCollision(Transform target) {
		_transform.position += (target.position - _transform.position).normalized * _currentSpeed * Time.deltaTime;
		_transform.rotation = Quaternion.LookRotation(target.position - _transform.position);
	}
	
	public void MoveTowardsTargetIgnoreCollision(GameObject target) {
		MoveTowardsTargetIgnoreCollision(target.transform);
	}
	
	public bool IsWithinTargetsReach(Transform target) {
		Debug.Log (Vector3.Distance(_transform.position, target.transform.position) + " " + _currentSpeed * Time.deltaTime);
		return Vector3.Distance(_transform.position, target.transform.position) < _currentSpeed * Time.deltaTime;	
	}
	
	public bool IsWithinTargetsReach(GameObject target) {
		return IsWithinTargetsReach(target.transform);	
	}
	
	void Update() {
		/*if (_action == null) return;
		
		Vector3 motion = Vector3.zero;
		
		switch(_action.type) {
		case ControllerState.Move:
			MoveAction move_action = (MoveAction) _action;
			Vector3 target_position = move_action._targets.First.Value.position;
			motion += (target_position - _transform.position).normalized * _currentSpeed * Time.deltaTime;
			_transform.rotation = Quaternion.LookRotation(target_position - _transform.position);
			break;
		}*/
		
		_charController.Move(_motion);
		_motion = Vector3.zero;
	}
}

/*enum ControllerState {
	Move
}
		
abstract class ControllerAction {
	public ControllerState type;
}

class MoveAction : ControllerAction {
	public LinkedList<Transform> _targets;
	
	public MoveAction(LinkedList<Transform> targets) {
		this.type = ControllerState.Move;
		this._targets = targets;
	}
}*/