using UnityEngine;
using System.Collections.Generic;

public class EquipableItem : Item {
	private Interval<int> _durability;
	private Buff _stats;
	protected ItemSlot _slot;
	
#region setters/getters
	public int MaxDurability {
		get {
			return _durability.Max;	
		}
		set {
			_durability.Max = value;
		}
	}
	
	public int Durability {
		get {
			return _durability.Cur;	
		}
		set {
			_durability.Cur = value;
		}
	}
	
	public ItemSlot Slot {
		get {
			return _slot;	
		}
	}
#endregion
	
	public void Equip(BaseCharacter bc) {
		_stats.Apply(bc);
	}
	
	public List<KeyValuePair<string, int>> GetStats() {
		return _stats.GetStats();	
	}
	
	public bool IsBroken() {
		if (_durability == null) return false;
		return _durability.Cur == 0;	
	}
}

public enum ItemSlot {
	Head,
	RightHand,
	LeftHand,
	TwoHands
}