using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : EquipableItem {
	private List<Damage> _dmgList;
	private float _range;
	private float _speed;
	
	public Weapon() {
		_dmgList = new List<Damage>();
		_range = 0;
		_speed = 0;
	}
	
	public Weapon(float range, float speed, List<Damage> dmgList) {
		_range = range;
		_dmgList = dmgList;
		_speed = speed;
	}
	
	public List<Damage> GetDamageList() {
		return _dmgList;	
	}
	
	public float Range {
		get {
			return _range;	
		}
		set {
			_range = value;	
		}
	}
	
	public float AttackSpeed {
		get {
			return _speed;	
		}
		set {
			_speed = value;	
		}
	}
}

