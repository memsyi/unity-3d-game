using System.Collections.Generic;

public static class WeaponsPackage {
	private static List<Damage> BasicSwordDmg = new List<Damage>(new Damage[1]{new Damage(new MinMaxInterval<int>(1,2), DamageType.Slashing)});
	public static Weapon BasicSword = new Weapon(2.5f, 1.6f, BasicSwordDmg);
	
	private static List<Damage> BasicAxeDmg = new List<Damage>(new Damage[2]{
																			new Damage(new MinMaxInterval<int>(2,3), DamageType.Slashing),
																			new Damage(new MinMaxInterval<int>(2,3), DamageType.Crushing)
																});
	public static Weapon BasicAxe = new Weapon(2.5f, 1.167f, BasicAxeDmg);
}
