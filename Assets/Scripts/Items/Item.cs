using UnityEngine;

public class Item {
	private string _name;
	private int _value;
	private RarityType _rarity;
	private Interval<int> _stack;
	
	public Item() {
		_name = "Unnamed item";
		_value = 0;
		_rarity = RarityType.Common;
		_stack = new Interval<int>(0,0,1);
	}
	
#region setters/getters
	public string Name {
		get { return _name; }
		set { _name = value; }
	}
	
	public int Value {
		get { return _value; }
		set { _value = value; }
	}
	
	public RarityType Rarity {
		get { return _rarity; }
		set { _rarity = value; }
	}
	
	public int StackSize {
		get { return _stack.Cur; }
		set { 
			_stack.Cur = value;
		}
	}
	
	public int StackLimit {
		get { return _stack.Max; }
		set {
			_stack.Max = value;
		}
	}
#endregion

	
}

public enum RarityType {
	Common,
	Uncommon,
	Rare,
	Legendary
}