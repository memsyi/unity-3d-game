/// <summary>
/// Health energy handler.
/// Script that makes sure, that current health value / max health value is always higher than
/// current energy value / max energy value
/// </summary>
using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BaseCharacter))]
public class HealthEnergyHandler : MonoBehaviour {
	
	private Vital _energy;
	private Vital _health;
	private BaseCharacter _character;
	
	void Awake() {
		_character = GetComponent<BaseCharacter>();
		_energy = _character.GetVital((int)VitalName.Energy);
		_health = _character.GetVital((int)VitalName.Health);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		_energy.CurValue = System.Math.Min(_energy.CurValue, (int)(_energy.AdjustedBaseValue * _health.CurValue / _health.AdjustedBaseValue));
	}
}
