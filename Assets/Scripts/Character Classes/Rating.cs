public class Rating : ModifiedStat {
	public Rating() {
		StartingExpToLevel = 25;
		LevelModifier = 1.1f;
	}
}

public enum RatingName {
	CrushingDefenseRating,
	PiercingDefenseRating,
	SlashingDefenseRating,
	FireResistance,
	FrostResistance,
	//Energy
	EnduranceRating,
	//Mana
	MagicalProtectionRating,
	//Willpower
	ForceOfWillRating
}
