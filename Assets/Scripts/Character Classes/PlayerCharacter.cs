using UnityEngine;

public class PlayerCharacter : SparkUser {
	private Weapon _mainHand;
	private Melee _melee;
	
	public PlayerCharacter() {
		MainHand = WeaponsPackage.BasicAxe;
	}
	
	public Weapon MainHand {
		get {
			return _mainHand;	
		}
		set {
			_mainHand = value;
			_melee = new Melee(_mainHand);
		}
	}
	
	public Melee Melee {
		get {
			return _melee;
		}
	}
}
