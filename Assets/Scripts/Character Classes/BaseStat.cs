using System;

public class BaseStat {
	private int _baseValue;
	private int _buffValue;
	private int _expToLevel;
	private int _startingExpToLevel;
	private float _levelModifier;
	private string _name;
	
	public BaseStat() {
		_name = "";
		_baseValue = 0;
		_buffValue = 0;
		_startingExpToLevel = 10;
		_expToLevel = CalculateExpToLevel();
		_levelModifier = 1.1f;
	}

#region Setters/Getters
	public int BaseValue {
		get { return _baseValue; }
		set { _baseValue = value; }
	}
	
	public int BuffValue {
		get { return _buffValue; }
		set { _buffValue = value; }
	}
	
	public int ExpToLevel {
		get { return _expToLevel; }
	}
	
	public int StartingExpToLevel {
		get { return _startingExpToLevel; }
		set {
			_startingExpToLevel = value; 
			_expToLevel = CalculateExpToLevel();
		}
	}
	
	public float LevelModifier {
		get { return _levelModifier; }
		set { _levelModifier = value; }
	}
	
	public string Name {
	 	get { return _name; }
		set { _name = value; }
	}
#endregion
	
	public int LevelUp() {
		int tmp = _expToLevel;
		_baseValue++;
		_expToLevel = CalculateExpToLevel();
		return tmp;
	}
	
	public int LevelDown() {
		_baseValue--;
		_expToLevel = CalculateExpToLevel();
		return _expToLevel;
	}
	
	public int AdjustedBaseValue {
		get {return _baseValue + _buffValue; }	
	}
	
	protected virtual int CalculateExpToLevel() {
		return (int)(_startingExpToLevel * Math.Pow(_levelModifier, _baseValue));	
	}
}
