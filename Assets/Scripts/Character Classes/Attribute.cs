public class Attribute : BaseStat {
	
	public Attribute(string name) {
		Name = name;
		StartingExpToLevel = 1;
		LevelModifier = 1.05f;
	}
}

public enum AttributeName {
	Strength,
	Constitution,
	Dexterity,
	Speed,
	Concentration,
	Power,
	Charisma
}