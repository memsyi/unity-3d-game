using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animation))]
public class PlayerController : MonoBehaviour {
	public float moveSpeed = 9;
	public float strafeSpeed = 6;
	public float rotationSpeed = 80;
	public float gravity = 10;
	public float minJumpHeight = 1.5f;
	public float maxJumpHeight = 2.5f;
	
	private Transform _transform;
	private CharacterController _controller;
	private CharacterAnimator _animator;
	private PlayerCharacter _pc;
	private Targetting _targetting;
	private Vector3 _moveDirection;
	private float _currentJump;
	private bool _isJumping;
	private CollisionFlags _collisionFlags;
	private float _fallingSpeed;
	
	void Awake () {
		_transform = transform;
		_controller = GetComponent<CharacterController>();
		_animator = GetComponent<CharacterAnimator>();
		_pc = GetComponent<PlayerCharacter>();
		_targetting = GetComponent<Targetting>();
	}
	
	void Start () {
		_animator.PlayIdle();
	}
	
	// Update is called once per frame
	void Update () {
		_moveDirection = Vector3.zero;
		Jump();
		Turn();
		Run();
		Strafe();
		Attack();
		Cast();
		if (_moveDirection == Vector3.zero && _animator.IsLooping()) _animator.CrossFadeIdle();
		ApplyGravity();
		ApplyMoves();
		Die();
	}
	
	private void Jump() {
		if ((_collisionFlags & CollisionFlags.CollidedAbove) != 0) _isJumping = false;
		if (Input.GetButton("Jump") && _currentJump < maxJumpHeight || 
			_isJumping && _currentJump < minJumpHeight) {
			if (_controller.isGrounded) {
				_isJumping = true;
			}
			if (_isJumping) {
				_moveDirection.y += (2 * maxJumpHeight - _currentJump) * Time.deltaTime; 
				_currentJump += (2 * maxJumpHeight - _currentJump) * Time.deltaTime; 
			}
		}
		else {
			_isJumping = false;
			_currentJump = 0;
		}
	}

	private void Turn () {
		if (Input.GetAxis("Rotate Character") != 0) {
			_transform.Rotate(0, Input.GetAxis("Rotate Character") * Time.deltaTime * rotationSpeed, 0);
		}
	}

	private void Run () {
		if (Input.GetButton("Move Forward")) {
			_animator.CrossFadeRun();
			float axis = Mathf.Max (-0.5f, Input.GetAxis("Move Forward"));
			_moveDirection += (_transform.forward * axis * Time.deltaTime * moveSpeed);
		}
		if (Input.GetButton("Rotate Camera") && Input.GetButton("Mouse Move")) {
			_animator.CrossFadeRun();
			_moveDirection += _transform.forward * Input.GetAxis("Mouse Move") * Time.deltaTime * moveSpeed;	
		}
	}

	private void Strafe () {
		if (Input.GetAxis("Strafe") != 0) {
			_animator.CrossFadeRun();
			_moveDirection += (_transform.right * Input.GetAxis("Strafe") * strafeSpeed * Time.deltaTime);
		}
	}
	
	private void Attack() {
		if (Input.GetButton("Attack")) {
			if (_targetting.SelectedTarget == null) {
				if (Locker.CheckAndLock("NoTargetLock", 3)) return;
				UIHandler.ShowText("I don't have a target", 3);
				return;
			}
			if (_pc.Melee.IsReachable(_transform, _targetting.SelectedTarget.transform) == TargetLocation.OutOfRange) {
				if (Locker.CheckAndLock("OutOfRangeLock", 3)) return;
				UIHandler.ShowText("I'm out of range", 3);
				return;
			}
			else if (_pc.Melee.IsReachable(_transform, _targetting.SelectedTarget.transform) == TargetLocation.NotInLineOfSight) {
				if (Locker.CheckAndLock("NotInLineOfSightLock", 3)) return;
				UIHandler.ShowText("Target not in line of sight", 3);
				return;
			}
			else if (!_pc.Melee.IsReady()) {
				if (Locker.CheckAndLock("NotReadyLock", 3)) return;
				UIHandler.ShowText("I'm not ready yet", 3);
				return;
			}
			else if (!_targetting.SelectedTarget.GetComponent<BaseCharacter>().IsAlive()){
				if (Locker.CheckAndLock("DeadTargetLock", 3)) return;
				UIHandler.ShowText("The target is dead", 3);
				return;
			}
			else {
				_animator.CrossFadeMelee();
				_pc.Melee.Perform(gameObject, _targetting.SelectedTarget);
			}	
		}
	}
	
	private void Cast() {
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			Spell s = new Spell();
			s.areaOfEffect = AreaOfEffect.SingleTarget;
			s.spellTargetEffects = new System.Collections.Generic.List<SparkPowerPair>{new SparkPowerPair(new sparks.fire.FireDamage(), 1)};
			s.Cast(_pc, _targetting.SelectedTarget.GetComponent<BaseCharacter>());
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			Spell s = new Spell();
			s.areaOfEffect = AreaOfEffect.TargetArea;
			s.areaModifier = 1;
			s.spellTargetEffects = new System.Collections.Generic.List<SparkPowerPair>{new SparkPowerPair(new sparks.fire.FireDamage(), 1)};
			s.Cast(_pc, null);
		}
	}
	
	private void ApplyGravity () {
		if (_controller.isGrounded) _fallingSpeed = 0;
		else if (!_isJumping) _fallingSpeed += gravity * Time.deltaTime;
		_moveDirection.y -= _fallingSpeed * Time.deltaTime;
	}
	
	private void ApplyMoves() {
		_collisionFlags = _controller.Move (_moveDirection);	
	}
	
	private void Die() {
		if (_pc.GetVital((int)VitalName.Health).CurValue == 0) {
			_animator.PlayDie();
			enabled = false;
			UIHandler.ShowText("You are dead", 10);
		}
	}
}
