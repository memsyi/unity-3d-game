/// <summary>
/// Targetting.
/// 
/// This script will target anything with "Enemy" tag.
/// </summary>

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Targetting : MonoBehaviour {
	private List<Transform>[] _targets;
	private HashSet<Transform>[] _recentTargets;
	private Transform _selectedTarget;
	private Transform _transform;
	private UIHandler _UI;

	public GameObject SelectedTarget {
		get {
			if (_selectedTarget == null) return null;
			return _selectedTarget.gameObject;	
		}
	}
	
	private Transform myTransform;
		
	// Use this for initialization
	void Start () {
		_targets = new List<Transform>[Enum.GetValues(typeof(UnitType)).Length];
		_recentTargets = new HashSet<Transform>[Enum.GetValues(typeof(UnitType)).Length];
		foreach (UnitType type in Enum.GetValues(typeof(UnitType))) {
			_targets[(int)type] = new List<Transform>();
			_recentTargets[(int)type] = new HashSet<Transform>();
		}
		_transform = transform;
		_UI = GameObject.Find("UI").GetComponent<UIHandler>();
	}
	
	private void AddAllUnitsTagged(string tag) {
		GameObject[] go = GameObject.FindGameObjectsWithTag(tag);
		
		foreach (GameObject enemy in go) {
			this.AddTarget(enemy.transform, UnitType.Hostile);	
		}
	}
	
	private void QueueFrontTargetsInRange(float range, UnitType type) {
		_targets[(int)type].Clear();
		GameObject[] go = GameObject.FindGameObjectsWithTag(type.ToString());
		
		////Debug.Log("[TARGETTING] " + type.ToString() + " " + go + " " + go.Length);
		
		foreach (GameObject enemy in go) {
			//Debug.Log ("[TARGETTING] " + enemy.transform + " " + Vector3.Distance(_transform.position, enemy.transform.position) + " " + Vector3.Dot((enemy.transform.position - _transform.position).normalized, _transform.forward));
			if (Vector3.Distance(_transform.position, enemy.transform.position) < range &&
				Vector3.Dot((enemy.transform.position - _transform.position).normalized, _transform.forward) > 0.4f)
				AddTarget(enemy.transform, type);
		}
	}
		
	private void AddTarget(Transform target, UnitType type) {
		this._targets[(int)type].Add(target);	
	}
	
	private void SortTargetsByDistance(UnitType type) {
		this._targets[(int)type].Sort(delegate(Transform x, Transform y) {
			return Vector3.Distance(this.transform.position, x.position).CompareTo(Vector3.Distance(this.transform.position, y.position));
		});
	}
	
	private void TargetUnit(UnitType type) {
		QueueFrontTargetsInRange(20, type);
		if (_targets[(int)type].Count == 0) return;
		SortTargetsByDistance(type);
		
		//Debug.Log ("[TARGETTING] RECENT TARGETS " + _recentTargets[(int)type].Count);
		
		foreach(Transform unit in _targets[(int)type]) {
			if (_recentTargets[(int)type].Contains(unit)) continue;
			SelectTarget(unit, type);
			return;
		}
		
		//Debug.Log("[TARGETTING] NO NEW TARGETS");
		_recentTargets[(int)type].Clear();
		TargetUnit(type);
	}
	
	private void SelectTarget(Transform target, UnitType type) {
		_recentTargets[(int)type].Add(target);
		if (target == _selectedTarget) return;
		this.DeselectTarget();
		this._selectedTarget = target;
		//Debug.Log("[TARGETTING] ADDING TARGET TO RECENT TARGETS " + target);
		_UI.AssignTarget(target.GetComponent<BaseCharacter>());
		ShowTargetName();
		//Instantiate(selectionLight, target.position, target.rotation);
		this._selectedTarget.gameObject.AddComponent<Light>();
		this._selectedTarget.gameObject.light.type = LightType.Point;
	}
	
	private void DeselectTarget() {
		if (this._selectedTarget == null) return;
		_UI.AssignTarget(null);
		Destroy(_selectedTarget.gameObject.light);
		HideTargetName();
		this._selectedTarget = null;
	}

	private void ShowTargetName()
	{
		Transform name = _selectedTarget.FindChild("Name");
		
		if (name == null) {
			//Debug.LogError("Could not find \"Name\" object on " + _selectedTarget.name);
			return;
		}
		
		name.GetComponent<MeshRenderer>().enabled = true;
	}
	
	private void HideTargetName() {
		Transform name = _selectedTarget.FindChild("Name");
		
		if (name == null) {
			//Debug.LogError("Could not find \"Name\" object on " + _selectedTarget.name);
			return;
		}
		
		name.GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)) {
			this.TargetUnit(UnitType.Hostile);	
		}
		//if (_selectedTarget != null) 
		//	_selectedTarget.GetComponent<BaseCharacter>().DisplayHealth();
	}
}

public enum UnitType {
	Hostile,
	Friendly,
	Neutral
}