using UnityEngine;
using System.Collections;

public class PlayerHealthBar : VitalBar {
	public const string MESSAGE = "Player Health Changed";
	
	void Update() {
		Messenger<int, int>.Broadcast(MESSAGE, 70, 100);	
	}
	
	public override void OnEnable() {
		Messenger<int, int>.AddListener(MESSAGE, OnValueChanged);
	}
	
	public override void OnDisable() {
		Messenger<int, int>.RemoveListener(MESSAGE, OnValueChanged);
	}
}
