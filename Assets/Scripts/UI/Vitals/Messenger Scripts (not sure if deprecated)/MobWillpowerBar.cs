using UnityEngine;
using System.Collections;

public class MobWillpowerBar : VitalBar {
	public const string MESSAGE = "Mob Willpower Changed";
	
	public override void OnEnable() {
		Messenger<int, int>.AddListener(MESSAGE, OnValueChanged);
	}
	
	public override void OnDisable() {
		Messenger<int, int>.RemoveListener(MESSAGE, OnValueChanged);
	}
}
