using UnityEngine;
using System.Collections;

public class PlayerEnergyBar : VitalBar {
	public const string MESSAGE = "Player Energy Changed";
	
	void Update() {
		Messenger<int, int>.Broadcast(MESSAGE, 40, 100);	
	}
	
	public override void OnEnable() {
		Messenger<int, int>.AddListener(MESSAGE, OnValueChanged);
	}
	
	public override void OnDisable() {
		Messenger<int, int>.RemoveListener(MESSAGE, OnValueChanged);
	}
}
