using UnityEngine;
using System.Collections;

public class MobManaBar : VitalBar {
	public const string MESSAGE = "Mob Mana Changed";
	
	public override void OnEnable() {
		Messenger<int, int>.AddListener(MESSAGE, OnValueChanged);
	}
	
	public override void OnDisable() {
		Messenger<int, int>.RemoveListener(MESSAGE, OnValueChanged);
	}
}
