using System.Collections.Generic;
using UnityEngine;

public abstract class Spark {
	protected SparkTarget _sparkTarget;
	
	public SparkTarget PossibleTargets {
		get {
			return _sparkTarget;
		}
	}
	
	public abstract string Name();
	public abstract string Description();
	
	public abstract SpellCost Cost(int power);
	public void Use(SparkUser caster, List<BaseCharacter> targets, Vector3 area, int power) {
		// Do stuff, like save usage statistics
		
		OnUse(caster, targets, area, power);
	}
	
	protected abstract void OnUse(SparkUser caster, List<BaseCharacter> targets, Vector3 area, int power);
	public abstract float Cooldown();
	public abstract bool IsUsable(BaseCharacter target, Transform area, int power);
}

public enum SparkTarget {
	Friendly,
	Enemy,
	Self,
	Any
}
