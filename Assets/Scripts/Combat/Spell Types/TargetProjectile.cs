using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(BaseMovementController))]
public class TargetProjectile : BasicSpellType {
	public BaseCharacter _target;
	
	private BaseMovementController _movementController;
	
#region setters/getters
	public float Speed {
		get { return _movementController.BaseMovementSpeed; }
		set { _movementController.BaseMovementSpeed = value; }
	}
#endregion
	
	void Awake() {
		this.enabled = false;
	}
	
	public void Setup(List<SparkPowerPair> effects, BaseCharacter target) {
		this.Effects = effects;
		this._target = target;
		_movementController = GetComponent<BaseMovementController>();
		Speed = 10;
		this.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (_movementController.IsWithinTargetsReach(_target.hitSpot)) {
			this.ApplySparksToTarget();
			this.Die();
		}
		else {
			_movementController.MoveTowardsTargetIgnoreCollision(_target.hitSpot);
		}
	}
	
	protected void ApplySparksToTarget() {
		foreach (SparkPowerPair rp in this.Effects) {
			rp.spark.Use(this.Caster, new List<BaseCharacter>{_target}, Vector3.zero, rp.power);
		}
	}
	
	private void Die() {
		Destroy(gameObject);	
	}
}
