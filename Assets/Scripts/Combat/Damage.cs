using UnityEngine;
using System.Collections;

public class Damage {
	private MinMaxInterval<int> _dmgValue;
	private DamageType _dmgType;
	
	public Damage(MinMaxInterval<int> dmgValue, DamageType dmgType) {
		_dmgValue = dmgValue;
		_dmgType = dmgType;
	}
	
	public DamageType DmgType {
		get {
			return _dmgType;
		}
	}
	
	public int DamageValue() {
		return Random.Range(_dmgValue.Min, _dmgValue.Max+1);
	}
}

public enum DamageType {
	//Health
	Crushing,
	Piercing,
	Slashing,
	Fire,
	Frost,
	TrueDamage,
	//Energy
	Weakening,
	TrueWeaken,
	//Mana
	ManaBurn,
	TrueManaBurn,
	//Willpower
	Spiritual,
	TrueSpiritual
}
