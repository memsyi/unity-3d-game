using UnityEngine;
using System.Collections;

public class Melee : Attack {
	
	private float _cooldown;
	private float _lastAttack;
	
	private Weapon _weapon;
	
	public Melee(Weapon weapon) {
		_weapon = weapon;
	}
	
	public override bool IsReady() {
		return (Time.time - _lastAttack >= _weapon.AttackSpeed);	
	}
	
	public override void Perform(GameObject attacker, GameObject target) {
		if (target.GetComponent<BaseCharacter>().IsAlive() == false) return;
		_lastAttack = Time.time;
		attacker.GetComponent<CharacterAnimator>().CrossFadeMelee();
		target.GetComponent<CharacterAnimator>().CrossFadeGetHit();
		
		BaseCharacter targetBC = target.GetComponent<BaseCharacter>();
		
		foreach (Damage d in _weapon.GetDamageList()) {
			targetBC.ApplyDamage(d);	
		}
		
		//Debug.Log("[MELEE] Dealing " + (int)(dmg * def / (def + 100)) + " dmg");
	}
	
	public override TargetLocation IsReachable(Transform attacker, Transform target) {
		if (Vector3.Distance(attacker.position, target.position) > _weapon.Range) return TargetLocation.OutOfRange;
		if (Vector3.Dot(target.transform.position - attacker.transform.position, attacker.transform.forward) < 0) return TargetLocation.NotInLineOfSight;
		return TargetLocation.Reachable;
	}
}
