using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	public Transform target;
	public float moveSpeed = 0.03f;
	public int rotationSpeed = 1;
	public float minDistance = 2.0f;
	
	private Transform myTransform;
	
	void Awake() {
		myTransform = transform;
	}

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.FindGameObjectWithTag("Player");
		target = go.transform;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawLine(target.position, myTransform.position, Color.yellow);
	
		// Look at target
		myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
	
		// Move towards target
		if (Vector3.Distance(target.position, myTransform.position) > minDistance)		
			myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
	}
}
