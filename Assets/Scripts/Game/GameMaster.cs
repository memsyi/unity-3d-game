using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {
	public GameObject playerCharacter;
	public GameObject userInterface;
	
	private GameObject _pc;
	private GameObject _ui;
	private PlayerCharacter _pcScript;
	private UIHandler _uiScript;

	// Use this for initialization
	void Start () {
		InstantiatePlayerCharacter();
		
		LoadCharacter();
		
		InstantiateUI();
	}

	private void InstantiatePlayerCharacter ()
	{
		GameObject sp = GameObject.Find(GameSettings.PLAYER_SPAWN_POINT);
		if (sp == null) {
			Debug.LogWarning("Could not find a spawn point for player! Creating one at (0,0,0).");
			sp = new GameObject(GameSettings.PLAYER_SPAWN_POINT);
		}
		
		_pc = Instantiate(playerCharacter, sp.transform.position, sp.transform.rotation) as GameObject;
		_pc.name = "Player Character";
		_pcScript = _pc.GetComponent<PlayerCharacter>();
		
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().target = _pc.transform.FindChild("CameraTarget");
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>().enabled = true;
	}
	
	private void InstantiateUI() {
		_ui = Instantiate(userInterface, Vector3.zero, Quaternion.identity) as GameObject;
		_ui.name = "UI";

		_uiScript = _ui.GetComponent<UIHandler>();
		_uiScript.AssignPlayer(_pcScript);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void LoadCharacter() {
		GameObject.Find("_GameSettings").GetComponent<GameSettings>().LoadCharacterData();
	}
	
	public static Vector3 GetMousePosition() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit rh;
		if (Physics.Raycast(ray, out rh)) {
			Debug.Log(rh.point);
		}
		return rh.point;
	}
}
